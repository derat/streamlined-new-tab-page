# streamlined-new-tab-page

This extension replaces Chrome's new tab page with one that attempts to be
similar to the default, minus all the junk that Google keeps adding that makes
the page worse.

More specifically, the page includes bookmarks and shortcuts, but there's no
enormous Google logo, redundant search field, Google account header, network
requests that spam the JavaScript console with warnings, or "Create theme with
AI" promotions.

<p float="left">
  <img src="https://www.erat.org/programming/ntp_light-1280.png" width="48%" alt="Bookmark menu and shortcuts">
  <img src="https://www.erat.org/programming/ntp_dark-1280.png" width="48%" alt="Settings">
</p>

The extension is installable from the Chrome Web Store as
[Streamlined New Tab Page](https://chromewebstore.google.com/detail/streamlined-new-tab-page/kndefdbjkflgdnhfkoagjaiifedhhbfc).
