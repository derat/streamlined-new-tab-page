// Copyright 2024 Daniel Erat.
// All rights reserved.

import {
  $,
  $t,
  cloneTemplate,
  getFaviconUrl,
  openUrlOnClickIfRestricted,
} from './common.js';
import { MenuTree, showContextMenu } from './menu.js';
import type { MenuItem } from './menu.js';
import { getSettings } from './settings.js';

const BAR_ID = '1'; // "Bookmarks Bar" folder
const OTHER_ID = '2'; // "Other Bookmarks" folder

// Currently-open bookmarks (not context) menu.
let menu: MenuTree | null = null;

// Updates the page's bookmarks bar.
export async function updateBookmarksBar() {
  menu?.close();
  menu = null;

  updateBookmarksBarVisibility();

  // Rebuild the bar even if it's hidden so it'll have the current items if it's
  // shown later.
  const bar = $('bookmarks-bar');

  // Adds a context menu listener to |el|.
  const addContext = (el: HTMLElement, id: string) => {
    el.addEventListener('contextmenu', (ev) => {
      showContextMenu(makeContextMenuItems(id), ev, el, 'context-shown');
      ev.preventDefault();
    });
  };

  // Adds an item (i.e. bookmark) to the bookmarks bar.
  const addItem = (id: string, title: string, url: string) => {
    const el = cloneTemplate('item-template', bar, {
      'a; href': url,
      'a; title': title + '\n' + url,
      '.title': title,
      '.icon; src': getFaviconUrl(url, 16),
    }) as HTMLElement;
    el.tabIndex = 0;
    openUrlOnClickIfRestricted(el, url);
    addContext(el, id);
  };

  // Adds a folder to the bookmarks bar.
  const addFolder = (id: string, title: string, right: boolean) => {
    const el = cloneTemplate('folder-template', bar, {
      '.title': title,
    }) as HTMLElement;
    el.tabIndex = 0;
    addContext(el, id);

    const open = async (fromKeyboard: boolean) => {
      el.classList.add('open');
      const tree = await chrome.bookmarks.getSubTree(id);
      const items = tree[0].children!.map((n) => makeMenuItem(n));
      menu = new MenuTree(
        items,
        el.getBoundingClientRect(),
        right,
        (m: MenuTree) => {
          el.classList.remove('open');
          if (menu === m) menu = null;
        },
        null /* menuClass */,
        fromKeyboard /* highlightFirst */
      );
    };
    el.addEventListener('click', () => open(false));
    el.addEventListener('keydown', (ev) => {
      if (ev.key === 'Enter' || ev.key === 'ArrowDown') open(true);
    });
  };

  const bookmarks = await chrome.bookmarks.getChildren(BAR_ID);
  bar.textContent = '';
  for (let n of bookmarks) {
    n.url ? addItem(n.id, n.title, n.url) : addFolder(n.id, n.title, false);
  }

  const spacer = document.createElement('div');
  spacer.classList.add('spacer');
  bar.appendChild(spacer);

  addFolder(OTHER_ID, $t('other_bookmarks'), true);
}

// Shows or hides the bookmarks bar depending on preferences.
export function updateBookmarksBarVisibility() {
  $('bookmarks-bar').classList.toggle('hidden', !getSettings().showBookmarks);
}

// Returns a menu item for showMenu().
function makeMenuItem(n: chrome.bookmarks.BookmarkTreeNode): MenuItem {
  const item: MenuItem = {
    title: n.title,
    contextItems: () => makeContextMenuItems(n.id),
  };
  if (n.url) {
    item.url = n.url;
    item.img = getFaviconUrl(n.url, 16);
  }
  if (n.children) item.children = n.children.map((c) => makeMenuItem(c));
  return item;
}

// Returns menu items to display in a context menu for the specified bookmark or
// folder.
function makeContextMenuItems(id: string): MenuItem[] {
  return [
    {
      title: $t('edit_ellipsis'),
      cb: () => {
        // Chrome logs a "Not allowed to load local resource" error if we
        // attempt to navigate to a chrome://bookmarks URL:
        // https://stackoverflow.com/q/73394254
        // The bookmark editor also only seems to accept IDs of folders, so pass
        // the parent's ID if we're editing a bookmark.
        chrome.bookmarks.get(id).then((nodes) => {
          if (!nodes.length) return;
          const editId = nodes[0].url ? nodes[0].parentId ?? '' : nodes[0].id;
          chrome.tabs.update({ url: `chrome://bookmarks/?id=${editId}` });
        });

        // Close the menu to make sure we aren't displaying stale bookmarks if
        // the user makes changes.
        menu?.close();
        menu = null;
      },
    },
  ];
}

// Invokes |cb| when direct children of the "Bookmarks Bar" folder are modified.
export function listenForBookmarksBarChanges(cb: () => void) {
  chrome.bookmarks.onChanged.addListener(async (id: string) => {
    const nodes = await chrome.bookmarks.get(id);
    if (nodes.length && nodes[0].parentId === BAR_ID) cb();
  });
  chrome.bookmarks.onChildrenReordered.addListener((id: string) => {
    if (id === BAR_ID) cb();
  });
  chrome.bookmarks.onCreated.addListener((_, bookmark) => {
    if (bookmark.parentId === BAR_ID) cb();
  });
  chrome.bookmarks.onMoved.addListener((_, info) => {
    if (info.parentId === BAR_ID || info.oldParentId === BAR_ID) cb();
  });
  chrome.bookmarks.onRemoved.addListener((_, info) => {
    if (info.parentId === BAR_ID) cb();
  });
}
