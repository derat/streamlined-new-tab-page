// Copyright 2024 Daniel Erat.
// All rights reserved.

import { cloneTemplate, openUrlOnClickIfRestricted } from './common.js';

// MenuItem describes an item in a menu.
export type MenuItem = {
  title: string;
  url?: string;
  children?: MenuItem[];
  cb?: () => void;
  img?: string;
  contextItems?: () => MenuItem[];
};

// Delay before opening or closing submenus in response to pointer movement.
const UPDATE_ACTIVE_TIMEOUT_MS = 500;

// Context menu offset from click position.
const CONTEXT_OFFSET_X = 2;
const CONTEXT_OFFSET_Y = 2;

// Menu position relative to anchor.
enum AnchorSide {
  BOTTOM_LEFT = 0,
  BOTTOM_RIGHT,
  RIGHT_TOP,
  LEFT_TOP,
}

// MenuTree displays a tree of menu containing |items|.
// The root menu is anchored to to the left side of |anchor|'s bottom edge.
// If |expandLeft| is true, submenus open to the left instead of the right.
// If |closeCb| is non-null, it is invoked when the tree is closed.
export class MenuTree {
  #dialog = document.createElement('dialog');
  #expandLeft: boolean;
  #menuClass: string | null;
  #menus: Menu[] = []; // open menus starting with root
  #highlight: Highlight | null = null;
  #elementItems = new Map<HTMLElement, MenuItem>();
  #updateOpenTimeoutId: number | null = null;

  constructor(
    items: MenuItem[],
    anchor: DOMRect,
    expandLeft: boolean,
    closeCb: ((menu: MenuTree) => void) | null = null,
    menuClass: string | null = null,
    highlightFirst = false
  ) {
    this.#expandLeft = expandLeft;
    this.#menuClass = menuClass;

    this.#dialog.classList.add('menu-wrapper');
    document.body.appendChild(this.#dialog);

    this.#dialog.addEventListener('close', () => {
      document.body.removeChild(this.#dialog);
      if (closeCb) closeCb(this);
    });
    this.#dialog.addEventListener('click', (e) => {
      for (const menu of this.#menus) {
        const rect = menu.element.getBoundingClientRect();
        if (
          e.clientX >= rect.left &&
          e.clientX <= rect.right &&
          e.clientY >= rect.top &&
          e.clientY <= rect.bottom
        ) {
          return;
        }
      }
      this.close();
    });
    this.#dialog.addEventListener('keydown', (ev) => {
      if (this.#handleKeyDown(ev)) {
        ev.preventDefault();
        ev.stopPropagation();
      }
    });

    const root = this.#createMenu(
      { title: '', children: items },
      anchor,
      AnchorSide.BOTTOM_LEFT
    );
    this.#dialog.tabIndex = 0;
    this.#menus.push(root);

    if (highlightFirst) this.#setHighlight(root, 0);

    this.#dialog.appendChild(root.element);
    this.#dialog.showModal();
  }

  get #lastMenu(): Menu {
    return this.#menus[this.#menus.length - 1];
  }

  // Closes the root menu and any submenus.
  close() {
    this.#dialog.close();
  }

  // Creates and returns a menu for |parent| at the supplied position.
  #createMenu(parent: MenuItem, anchor: DOMRect, side: AnchorSide): Menu {
    if (!parent.children) throw new Error(`"${parent.title}" has no children`);
    const menu: Menu = {
      element: cloneTemplate('menu-template') as HTMLElement,
      item: parent,
    };
    if (this.#menuClass) menu.element.classList.add(this.#menuClass);

    for (let idx = 0; idx < parent.children.length; idx++) {
      const child = parent.children[idx];
      let el: HTMLElement;
      if (child.url || child.cb) {
        el = cloneTemplate('item-template', menu.element, {
          'a; href': child.url,
          'a; title': child.url ? child.title + '\n' + child.url : null,
          '.title': child.title,
          '.icon; src': child.img,
        }) as HTMLElement;

        el.addEventListener('click', () => {
          this.close();
          if (child.cb) child.cb();
        });
        if (child.url) openUrlOnClickIfRestricted(el, child.url);
      } else {
        el = cloneTemplate('folder-template', menu.element, {
          '.title': child.title,
        }) as HTMLElement;
        el.addEventListener('click', () => this.#updateOpen(el));
      }

      this.#elementItems.set(el, child);

      // Listening for pointerenter here initially worked, but it seems to be
      // broken as of Chrome 125.0.6422.140.
      el.addEventListener('mouseenter', () => {
        this.#setHighlight(menu, idx);
        this.#scheduleUpdateOpen(el);
      });
      el.addEventListener('mouseleave', () => {
        this.#unsetHighlight(menu, idx);
      });

      if (child.contextItems) {
        el.addEventListener('contextmenu', (ev) => {
          this.#updateOpen(el);
          showContextMenu(child.contextItems!(), ev, el, 'context-shown');
          ev.preventDefault();
        });
      }
    }

    // Make sure we don't open or close submenus if the user later moved the
    // pointer outside the menu.
    menu.element.addEventListener('mouseleave', () => this.#cancelUpdateOpen());

    this.#moveMenu(menu, anchor, side);

    return menu;
  }

  // Update |menu|'s position relative to |anchor|.
  #moveMenu(menu: Menu, anchor: DOMRect, side: AnchorSide) {
    const el = menu.element;
    if (side === AnchorSide.BOTTOM_LEFT) {
      el.style.left = `${anchor.left}px`;
      el.style.top = `${anchor.bottom}px`;
    } else if (side === AnchorSide.BOTTOM_RIGHT) {
      el.style.right = `${window.innerWidth - anchor.right}px`;
      el.style.top = `${anchor.bottom}px`;
    } else if (side === AnchorSide.RIGHT_TOP) {
      el.style.left = `${anchor.right}px`;
      el.style.top = `${anchor.top}px`;
    } else if (side === AnchorSide.LEFT_TOP) {
      el.style.right = `${window.innerWidth - anchor.left}px`;
      el.style.top = `${anchor.top}px`;
    }

    // If the menu extends outside the window, move it onscreen.
    // Hide it until we've positioned it to avoid some jank.
    el.classList.add('hidden');
    window.setTimeout(() => {
      const rect = el.getBoundingClientRect();
      if (rect.height >= window.innerHeight) {
        el.style.top = '0px';
      } else if (rect.bottom > window.innerHeight) {
        el.style.top = 'auto';
        el.style.bottom = '0px';
      }
      if (rect.width >= window.innerWidth) {
        el.style.left = '0px';
      } else if (rect.right > window.innerWidth) {
        el.style.left = 'auto';
        el.style.right = '0px';
      }
      el.classList.remove('hidden');
    });
  }

  // Updates displayed submenus for the pointer being over |el| (corresponding
  // to a menu item).
  #updateOpen(el: HTMLElement) {
    const item = this.#elementItems.get(el);
    if (!item) throw new Error('Failed finding menu item for element');

    this.#cancelUpdateOpen();

    for (let i = this.#menus.length - 1; i >= 0; i--) {
      const menu = this.#menus[i];
      if (menu.item === item) return;

      const itemIdx = findChildIndex(menu, item);
      if (itemIdx >= 0) {
        [...menu.element.children].forEach((e) => e.classList.remove('open'));
        if (item.children) {
          const anchor = el.getBoundingClientRect();
          const side = this.#expandLeft
            ? AnchorSide.LEFT_TOP
            : AnchorSide.RIGHT_TOP;
          const newMenu = this.#createMenu(item, anchor, side);
          this.#dialog.appendChild(newMenu.element);
          this.#menus.push(newMenu);
          el.classList.add('open');
          // Clear the highlight since we'll already be highlighting the item
          // due to it having a submenu. This seems to match Chrome's behavior:
          // if you open a menu by hovering over an item, pressing the Down key
          // selects the top item in the new menu rather than moving the
          // highlighting to the the original item's sibling.
          this.#unsetHighlight(menu, itemIdx);
        }
        return;
      }

      this.#closeTopMenu();
    }

    throw new Error(`Didn't find menu containing ${item.title}`);
  }

  // Schedules a call to #updateOpen().
  #scheduleUpdateOpen(el: HTMLElement) {
    this.#cancelUpdateOpen();
    this.#updateOpenTimeoutId = window.setTimeout(() => {
      this.#updateOpenTimeoutId = null;
      this.#updateOpen(el);
    }, UPDATE_ACTIVE_TIMEOUT_MS);
  }

  // Cancels a previously-scheduled call to #updateOpen.
  #cancelUpdateOpen() {
    if (this.#updateOpenTimeoutId === null) return;
    window.clearTimeout(this.#updateOpenTimeoutId);
    this.#updateOpenTimeoutId = null;
  }

  // Closes the top menu in #menus.
  #closeTopMenu() {
    if (this.#menus.length <= 1) {
      throw new Error(`Only ${this.#menus.length} menu(s) open`);
    }
    const closed = this.#menus.pop()!;
    this.#dialog.removeChild(closed.element);
    [...closed.element.children].forEach((e) =>
      this.#elementItems.delete(e as HTMLElement)
    );

    const parent = this.#lastMenu;
    const idx = findChildIndex(parent, closed.item);
    if (idx < 0) {
      throw new Error(
        `"${closed.item.title}" not a child of "${parent.item.title}"`
      );
    }
    parent.element.children[idx].classList.remove('open');
  }

  // Highlights the item at |idx| in |menu|.
  #setHighlight(menu: Menu, idx: number) {
    idx = (idx + menu.item.children!.length) % menu.item.children!.length;
    if (menu === this.#highlight?.menu && idx === this.#highlight?.idx) return;
    if (this.#highlight) {
      this.#unsetHighlight(this.#highlight.menu, this.#highlight.idx);
    }
    menu.element.children[idx].classList.add('highlight');
    this.#highlight = { menu, idx };
  }

  // Stops highlighting the item at |idx| in |menu|.
  #unsetHighlight(menu: Menu, idx: number) {
    menu.element.children[idx].classList.remove('highlight');
    if (menu === this.#highlight?.menu && idx === this.#highlight?.idx) {
      this.#highlight = null;
    }
  }

  // Handles the supplied keydown event.
  // Returns true if the event was consumed and false otherwise.
  #handleKeyDown(ev: KeyboardEvent): boolean {
    switch (ev.key) {
      case 'ArrowUp': {
        // Highlight the previous item, or the last if none is highlighted.
        this.#setHighlight(
          this.#highlight?.menu ?? this.#lastMenu,
          (this.#highlight?.idx ?? 0) - 1
        );
        return true;
      }
      case 'ArrowDown': {
        // Highlight the next item, or the first if none is highlighted.
        this.#setHighlight(
          this.#highlight?.menu ?? this.#lastMenu,
          (this.#highlight?.idx ?? -1) + 1
        );
        return true;
      }
      case 'ArrowLeft':
      case 'Escape': {
        // If no submenus are open, let the Escape key close the root menu.
        if (this.#menus.length < 2) {
          if (ev.key === 'Escape') this.close();
          return true;
        }

        // If a submenu is open, close it.
        const item = this.#lastMenu.item;
        this.#closeTopMenu();

        // Move the highlight to the item that owns the menu we closed.
        // This is intentionally different from how Chrome's menus work: at least
        // on Chrome OS, the highlight just disappears, but that feels weird.
        const parent = this.#lastMenu;
        this.#setHighlight(parent, findChildIndex(parent, item));
        return true;
      }
      case 'ArrowRight':
      case 'Enter': {
        // Select the first item if none is selected.
        if (!this.#highlight) {
          this.#setHighlight(this.#lastMenu, 0);
          return true;
        }
        // Otherwise, open a submenu if the current item has children.
        // The Enter key can also activate the selected item.
        const menu = this.#highlight.menu;
        const item = menu.item.children![this.#highlight.idx];
        const el = menu.element.children[this.#highlight.idx] as HTMLElement;
        if (item.children) {
          this.#updateOpen(el as HTMLElement);
          this.#setHighlight(this.#menus[this.#menus.length - 1], 0);
        } else if (ev.key === 'Enter') {
          // Do all this nonsense instead of just calling click() so that we can
          // preserve the state of modifier keys. See also
          // https://html.spec.whatwg.org/multipage/webappapis.html#event-firing.
          el.dispatchEvent(
            new PointerEvent('click', {
              bubbles: true,
              cancelable: true,
              composed: true,
              ctrlKey: ev.ctrlKey,
              shiftKey: ev.shiftKey,
              altKey: ev.altKey,
              metaKey: ev.metaKey,
              view: window,
            })
          );
        }
        return true;
      }
      default:
        return false;
    }
  }
}

// Menu contains information about an open menu (i.e. the root menu or a
// submenu).
interface Menu {
  element: HTMLElement;
  item: MenuItem;
}

// Returns the index of |item| among |menu.item.children| or -1 if not present.
const findChildIndex = (menu: Menu, item: MenuItem) =>
  menu.item.children?.findIndex((it) => it === item) ?? -1;

// Highlight contains information about the currently-highlighted menu item.
interface Highlight {
  menu: Menu; // menu containing the highlighted item
  idx: number; // index into |menu.item.children|
}

// Displays a context menu containing |items| positioned according to
// |anchorOrEvent|. If |classElement| is supplied, |className| is added to it
// while the menu is open.
export function showContextMenu(
  items: MenuItem[],
  anchorOrEvent: Element | MouseEvent,
  classElement: Element | null = null,
  className = 'context-shown'
) {
  const anchor =
    'clientX' in anchorOrEvent
      ? getContextMenuAnchor(anchorOrEvent as MouseEvent)
      : (anchorOrEvent as Element).getBoundingClientRect();
  classElement?.classList?.add(className);
  new MenuTree(
    items,
    anchor,
    false,
    () => classElement?.classList?.remove(className),
    'context'
  );
}

// Returns a rect used to position a context menu triggered by |ev|.
const getContextMenuAnchor = (ev: MouseEvent) =>
  DOMRect.fromRect({
    x: ev.clientX + CONTEXT_OFFSET_X,
    y: ev.clientY + CONTEXT_OFFSET_Y,
    width: 0,
    height: 0,
  });
