// Copyright 2024 Daniel Erat.
// All rights reserved.

export class Dialog {
  #wrapper = document.createElement('dialog');

  constructor(content: Element) {
    this.#wrapper.classList.add('dialog');
    document.body.appendChild(this.#wrapper);
    this.#wrapper.addEventListener('close', () => {
      document.body.removeChild(this.#wrapper);
    });
    this.#wrapper.appendChild(content);
    this.#wrapper.showModal();
  }

  close() {
    this.#wrapper.close();
  }
}
