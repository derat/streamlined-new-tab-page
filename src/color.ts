// Copyright 2024 Daniel Erat.
// All rights reserved.

// Color describes a color with 8-bit components.
export class Color {
  readonly r: number;
  readonly g: number;
  readonly b: number;
  readonly a: number;
  #luminance: number | null = null;

  constructor(r: number, g: number, b: number, a: number = 255) {
    this.r = r;
    this.g = g;
    this.b = b;
    this.a = a;
  }

  // Relative luminance in [0.0, 1.0] per
  // https://www.w3.org/TR/WCAG20/#relativeluminancedef.
  get luminance(): number {
    if (this.#luminance !== null) return this.#luminance;

    const conv = (v8: number) => {
      const vs = v8 / 255;
      return vs <= 0.03928 ? vs / 12.92 : Math.pow((vs + 0.055) / 1.055, 2.4);
    };
    return (this.#luminance =
      0.2126 * conv(this.r) + 0.7152 * conv(this.g) + 0.0722 * conv(this.b));
  }

  // Returns #rrggbbaa (for non-255 alpha) or #rrggbb.
  toHex(): string {
    const h = (v: number) => v.toString(16).padStart(2, '0');
    return (
      '#' + h(this.r) + h(this.g) + h(this.b) + (this.a < 255 ? h(this.a) : '')
    );
  }

  // Returns a copy of the color with updated alpha.
  copyWithAlpha = (a: number) => new Color(this.r, this.g, this.b, a);
}

// Parses a #rgb, #rgba, #rrggbb, or #rrggbbaa color.
// Returns null for invalid input.
export function parseColor(s: string): Color | null {
  if (!s.match(/^#[0-9a-f]{3,8}$/i)) return null;

  if (s.length === 4 || s.length == 5) {
    return new Color(
      parseInt(`${s[1]}${s[1]}`, 16),
      parseInt(`${s[2]}${s[2]}`, 16),
      parseInt(`${s[3]}${s[3]}`, 16),
      s.length === 4 ? 255 : parseInt(`${s[4]}${s[4]}`, 16)
    );
  } else if (s.length === 7 || s.length === 9) {
    return new Color(
      parseInt(s.slice(1, 3), 16),
      parseInt(s.slice(3, 5), 16),
      parseInt(s.slice(5, 7), 16),
      s.length === 7 ? 255 : parseInt(s.slice(7, 9), 16)
    );
  }
  return null;
}

export const WHITE = new Color(255, 255, 255);
export const BLACK = new Color(0, 0, 0);

// Computes the contrast ratio in [1.0, 21.0] between the supplied colors per
// https://www.w3.org/TR/WCAG20/#contrast-ratiodef.
//
// The same document recommends a minimum contrast ratio of at least 4.5 for
// text, or 3 for large-scale text.
export const getContrastRatio = (a: Color, b: Color) =>
  (Math.max(a.luminance, b.luminance) + 0.05) /
  (Math.min(a.luminance, b.luminance) + 0.05);
