#!/bin/bash -e

MESSAGES_FILE=_locales/en/messages.json
MANIFEST_FILE=manifest.json
PAGE_FILE=page.html
SRC_GLOB=src/*.ts

MSG_REGEXP='__MSG_[_a-zA-Z0-9@]+__'
MSG_SED='s/^__MSG_(.+)__/\1/'

defined=$(jq -r 'keys[] | .' "$MESSAGES_FILE" | sort | uniq)
used=$(
  (
    grep -Po "$MSG_REGEXP" "$MANIFEST_FILE" | sed -re "$MSG_SED" ;
    grep -Po 'data-i18n="[^"]+"' "$PAGE_FILE" | sed -re 's/.*"([^"]+)".*/\1/' ;
    grep -Po "$MSG_REGEXP" "$PAGE_FILE" | sed -re "$MSG_SED" ;
    # Why doesn't \b work properly here? And why am I writing a shell script?
    grep -Poh '(?s)\$t\('"'[^']+'"'\)' $SRC_GLOB | sed -re "s/.*'([^']+)'.*/\1/" ;
    grep -Poh "$MSG_REGEXP" $SRC_GLOB | sed -re "$MSG_SED"
  ) | sort | uniq
)

retval=0

check() {
  # We can get away with being lazy here since names can't contain spaces.
  local desc=$1
  local needles=$2
  local haystack=$3

  local -a missing
  for name in $needles; do
    if ! [[ " $haystack " =~ [[:space:]]${name}[[:space:]] ]]; then
      missing+=($name)
    fi
  done

  if [[ ${#missing[@]} -gt 0 ]]; then
    echo "$desc"
    for name in "${missing[@]}"; do echo "  $name"; done
    retval=1
  fi
}

check 'Undefined messages:' "$used" "$defined"
check 'Unused messages:' "$defined" "$used"
exit $retval
