// Copyright 2024 Daniel Erat.
// All rights reserved.

import { parseColor } from './color.js';
import { $, createShadow } from './common.js';

const HTML_CONTENT = `
<style>
  #wrapper {
    align-items: center;
    display: flex;
    gap: var(--margin);
  }
  #text-input {
    width: 100%;
  }
  #text-input.invalid {
    border-color: var(--control-invalid-color) !important;
    outline: solid 1px var(--control-invalid-color);
  }
  #color-input {
    height: 28px;
    width: 48px;
  }
</style>
<div id="wrapper">
  <input
    id="text-input"
    type="text"
    part="text-input"
    spellcheck="false"
    data-i18n
    placeholder="__MSG_color_placeholder__"
  />
  <input id="color-input" type="color" />
</div>
`;

// ColorInput displays a text input alongside the regrettable
// <input type="color"> component.
export class ColorInput extends HTMLElement {
  #shadow = createShadow(this, HTML_CONTENT);
  #textInput = $<HTMLInputElement>('text-input', this.#shadow);
  #colorInput = $<HTMLInputElement>('color-input', this.#shadow);

  // This stuff is weird:
  // https://www.dannymoerkerke.com/blog/web-components-can-now-be-native-form-elements/
  // https://stackoverflow.com/questions/32417235/how-to-make-a-custom-web-component-focusable
  static formAssociated = true;

  constructor() {
    super();

    this.addEventListener('focus', () => this.#textInput.focus());

    this.#textInput.addEventListener('change', () => {
      this.#textInput.value = this.#textInput.value.trim();
      const color = parseColor(this.#textInput.value);
      if (!color && this.#textInput.value !== '') {
        this.#textInput.classList.add('invalid');
        this.#colorInput.value = '#000000';
        return;
      }
      // <input type="color"> only accepts #rrggbb:
      // https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/color
      const hex = color?.toHex();
      this.#textInput.value = hex ?? '';
      this.#colorInput.value = hex?.slice(0, 7) ?? '#000000';
      this.#emitChangeEvent();
    });
    this.#textInput.addEventListener('input', () => {
      this.#textInput.classList.remove('invalid');
    });

    this.#colorInput.addEventListener('change', () => {
      this.#textInput.value = this.#colorInput.value;
      this.#textInput.classList.remove('invalid');
      this.#emitChangeEvent();
    });
  }

  connectedCallback() {
    if (!this.hasAttribute('tabindex')) this.setAttribute('tabindex', '0');
  }

  get value(): string {
    return parseColor(this.#textInput.value) ? this.#textInput.value : '';
  }
  set value(v: string | null) {
    v = (v ?? '').trim();
    const color = parseColor(v);
    if (!color && v !== '') {
      console.error(`Bad color "${v}"`);
      return;
    }
    const hex = color?.toHex();
    this.#textInput.value = hex ?? '';
    this.#textInput.classList.remove('invalid');
    this.#colorInput.value = hex?.slice(0, 7) ?? '#000000';
  }

  // Emits a 'change' event.
  #emitChangeEvent() {
    this.dispatchEvent(
      new CustomEvent('change', {
        bubbles: true,
        cancelable: false,
        composed: true,
      })
    );
  }
}

customElements.define('color-input', ColorInput);
