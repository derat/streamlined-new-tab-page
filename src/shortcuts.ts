// Copyright 2024 Daniel Erat.
// All rights reserved.

import { ColorInput } from './color-input.js';
import {
  $,
  $t,
  cloneTemplate,
  deepCopy,
  emptyImg,
  getFaviconUrl,
  inRect,
  openUrlOnClickIfRestricted,
  query,
} from './common.js';
import { Dialog } from './dialog.js';
import { MenuItem, showContextMenu } from './menu.js';
import { getSettings, updateSettings } from './settings.js';

// Shortcut describes a link to a site.
export interface Shortcut {
  name: string;
  url: string;
  color?: string; // icon background color
}

// Currently-displayed shortcuts. Changes should generally be made through
// getSettings() instead of by modifying this array.
let shortcuts: Readonly<Shortcut>[] = [];

// True after updateShortcuts() has been called.
let initialized = false;

// Updates the displayed shortcuts.
export async function updateShortcuts() {
  const cont = $('shortcuts');

  if (!initialized) {
    cont.addEventListener('dragover', (ev) => onDragOver(cont, ev));
    $('add-shortcut-button').addEventListener('click', () =>
      showEditDialog(-1)
    );
    initialized = true;
  }

  const wrapper = $('shortcuts-wrapper');
  if (!getSettings().showShortcuts) {
    wrapper.classList.remove('shown');
    return;
  }

  // Avoid rebuilding the shortcuts if they haven't changed.
  const newShortcuts = await getShortcuts();
  if (JSON.stringify(newShortcuts) !== JSON.stringify(shortcuts)) {
    shortcuts = newShortcuts;
    displayShortcuts(cont);
  }

  // Wait until after the shortcuts are displayed to show the wrapper.
  // Otherwise, there's some jank on page load where the add link is visible.
  wrapper.classList.add('shown');
}

// Reorders shortcuts in response to dragover events.
function onDragOver(cont: HTMLElement, ev: DragEvent) {
  ev.preventDefault();

  const ex = ev.clientX;
  const ey = ev.clientY;

  // Bail out in the common case where we're still in the dragged element.
  const dragging = query(cont, '.dragging');
  if (inRect(dragging.getBoundingClientRect(), ex, ey)) return;

  // Iterate through the shortcuts to find which one the cursor is in.
  let draggingIndex = -1;
  let destIndex = -1;
  let dest: Element | null = null;
  for (const [i, el] of [...cont.children].entries()) {
    if (el === dragging) {
      draggingIndex = i;
    } else if (inRect(el.getBoundingClientRect(), ex, ey)) {
      destIndex = i;
      dest = el;
    }
    if (draggingIndex >= 0 && destIndex >= 0) {
      const after = destIndex > draggingIndex;
      cont.insertBefore(dragging, after ? dest!.nextElementSibling : dest);
      const shortcut = shortcuts.splice(draggingIndex, 1);
      shortcuts.splice(destIndex, 0, ...shortcut);
      return;
    }
  }
}

// Gets the list of shortcuts to display.
async function getShortcuts(): Promise<Shortcut[]> {
  const settings = getSettings();
  if (settings.shortcuts) return deepCopy(settings.shortcuts);

  const sites = await chrome.topSites.get();
  return sites.map((s) => ({ name: s.title, url: s.url }));
}

// Regenerates the shortcut elements within |cont|.
function displayShortcuts(cont: HTMLElement) {
  cont.textContent = '';
  for (let shortcut of shortcuts) {
    const el = cloneTemplate('shortcut-template', cont, {
      'a; href': shortcut.url,
      'a; title': shortcut.name + '\n' + shortcut.url,
      '.icon; src': shortcut.url ? getFaviconUrl(shortcut.url, 32) : emptyImg,
      '.name': shortcut.name,
    }) as HTMLElement;

    if (shortcut.color) {
      query(el, '.icon').style.backgroundColor = shortcut.color;
    }

    openUrlOnClickIfRestricted(el, shortcut.url);

    // Show a context menu either when the menu button is clicked or the element
    // is right-clicked.
    const menuButton = query(el, '.menu-button')!;
    const items: MenuItem[] = [
      // Get the index dynamically in case the shortcuts have been reordered.
      {
        title: $t('edit_ellipsis'),
        cb: () => showEditDialog(shortcuts.indexOf(shortcut)),
      },
      {
        title: $t('remove'),
        cb: () => removeShortcut(shortcuts.indexOf(shortcut)),
      },
    ];
    menuButton.addEventListener('click', (ev) => {
      showContextMenu(items, menuButton, el, 'context-shown');
      ev.preventDefault();
      ev.stopPropagation();
    });
    menuButton.addEventListener('keydown', (ev) => {
      if (ev.key !== 'Enter' && ev.key !== 'ArrowDown') return;
      showContextMenu(items, menuButton, el, 'context-shown');
      ev.preventDefault();
      ev.stopPropagation();
    });
    el.addEventListener('contextmenu', (ev) => {
      showContextMenu(items, ev, el, 'context-shown');
      ev.preventDefault();
    });

    // Handle drag-and-drop related events.
    el.addEventListener('dragstart', (ev) => {
      ev.dataTransfer!.effectAllowed = 'move';
      el.classList.add('dragging');
    });
    el.addEventListener('dragend', () => {
      updateSettings((s) => (s.shortcuts = shortcuts));
      el.classList.remove('dragging');
      // Dumb hack to work around Chrome :hover bug; see shortcuts.scss.
      el.classList.add('dropped');
      cont.addEventListener(
        'pointermove',
        () => el.classList.remove('dropped'),
        { once: true }
      );
    });
  }
}

// Opens a dialog for editing a shortcut in |shortcuts|.
// If |index| is -1, a new shortcut is added instead.
function showEditDialog(index: number) {
  const shortcut = index >= 0 ? shortcuts[index] : { name: '', url: '' };
  const title = index < 0 ? $t('add_shortcut') : $t('edit_shortcut');
  const content = cloneTemplate('edit-shortcut-dialog-template', null, {
    '.title': title,
  }) as HTMLElement;
  const dialog = new Dialog(content);
  const nameInput = $<HTMLInputElement>('shortcut-name-input');
  const urlInput = $<HTMLInputElement>('shortcut-url-input');
  const colorInput = $<ColorInput>('shortcut-color-input');

  // Wait until the dialog gets the focus to set these, since the caret gets
  // placed at the beginning of |nameInput| rather than the end otherwise.
  nameInput.value = shortcut.name;
  urlInput.value = shortcut.url;
  colorInput.value = shortcut.color ?? '';

  for (const el of [nameInput, urlInput]) {
    el.addEventListener('input', () => el.classList.remove('invalid'));
  }

  const okButton = query<HTMLButtonElement>(content, '.ok-button');
  okButton.addEventListener('click', () => {
    let invalid = false;
    for (const el of [nameInput, urlInput]) {
      if (el.value.trim() === '') {
        el.classList.add('invalid');
        invalid = true;
      }
    }
    if (invalid) return;

    updateSettings((s) => {
      const edited: Shortcut = {
        name: nameInput.value.trim(),
        url: urlInput.value.trim(),
      };
      const color = colorInput.value.trim();
      if (color !== '') edited.color = color;

      // Update a shallow copy so updateShortcuts() won't skip the change.
      const copy = shortcuts.slice();
      if (index >= 0) copy[index] = edited;
      else copy.push(edited);
      s.shortcuts = copy;
    });

    dialog.close();
  });
  content.addEventListener('keydown', (ev) => {
    if (ev.key === 'Enter') {
      // preventDefault() (rather than stopPropagation()) strangely seems to be
      // necessary to prevent the event from re-triggering an empty dialog right
      // after we close it. I'm calling both because why not.
      ev.preventDefault();
      ev.stopPropagation();
      okButton.click();
    }
  });
  query(content, '.cancel-button').addEventListener('click', () =>
    dialog.close()
  );
}

// Removes a shortcut from |shortcuts|.
function removeShortcut(index: number) {
  updateSettings((s) => {
    const copy = shortcuts.slice();
    copy.splice(index, 1);
    s.shortcuts = copy;
  });
}
