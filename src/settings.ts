// Copyright 2024 Daniel Erat.
// All rights reserved.

import { WHITE, BLACK, getContrastRatio, parseColor } from './color.js';
import {
  $,
  cloneTemplate,
  deepCopy,
  deepFreeze,
  nullIfEmpty,
  query,
  sinceMs,
} from './common.js';
import { openDb } from './db.js';
import { Dialog } from './dialog.js';
import { Shortcut } from './shortcuts.js';
import type { ColorInput } from './color-input.js';

// Settings contains user-configurable settings stored in chrome.storage.sync.
export interface Settings {
  fgColor: string | null;
  bgColor: string | null;
  showBookmarks: boolean;
  showShortcuts: boolean;
  logToConsole: boolean;
  shortcuts: Shortcut[] | null; // used instead of top sites if non-null
}

// LocalSettings contains non-synnced settings stored in chrome.storage.local.
export interface LocalSettings {
  bgImageId: string | null; // opaque ID
  bgImagePlaceholder: string | null; // data: URL
}

// Key for settings objects in chrome.storage.sync and chrome.storage.local.
const SETTINGS_KEY = 'settings';

// Key for background image data in IndexedDB.
export const BG_IMAGE_KEY = 'bgImage';

// Default values for settings not present in chrome.storage.sync.
const DEFAULT_SETTINGS: Settings = Object.freeze({
  fgColor: null,
  bgColor: null,
  showBookmarks: true,
  showShortcuts: true,
  logToConsole: false,
  shortcuts: null,
});

// Default values for settings not present in chrome.storage.local.
const DEFAULT_LOCAL_SETTINGS: LocalSettings = Object.freeze({
  bgImageId: null,
  bgImagePlaceholder: null,
});

// Width in pixels for background image placeholder.
const PLACEHOLDER_WIDTH = 60;

// Minimum alpha allowed for foreground color.
const MIN_FG_COLOR_ALPHA = 192;

// Minimum contrast ratio between foreground and background colors before one of
// the colors gets switched to black or white. I'm just doing this to keep users
// from accidentally choosing colors that are so similar that they're unable to
// interact with the page to choose different colors.
const MIN_CONTRAST_RATIO = 2;

// Currently-loaded settings.
let settings: Readonly<Settings> | null = null;
let localSettings: Readonly<LocalSettings> | null = null;

// Loads settings from chrome.storage.sync and chrome.storage.local and watches
// for changes. |cb| is called after settings are loaded and whenever they have
// changed.
export function initSettings(cb: () => void) {
  if (settings) throw new Error('Settings already initialized');

  const loadSync = async () => {
    const res = await chrome.storage.sync.get(SETTINGS_KEY);
    settings = Object.assign({}, DEFAULT_SETTINGS, res[SETTINGS_KEY]);
    deepFreeze(settings);
  };
  const loadLocal = async () => {
    const res = await chrome.storage.local.get(SETTINGS_KEY);
    localSettings = Object.assign(
      {},
      DEFAULT_LOCAL_SETTINGS,
      res[SETTINGS_KEY]
    );
    deepFreeze(localSettings);
  };

  chrome.storage.sync.onChanged.addListener((changes: Record<string, any>) => {
    if (SETTINGS_KEY in changes) loadSync().then(() => cb());
  });
  chrome.storage.local.onChanged.addListener((changes: Record<string, any>) => {
    if (SETTINGS_KEY in changes) loadLocal().then(() => cb());
  });
  Promise.all([loadSync(), loadLocal()]).then(() => cb());
}

// Returns the current synced settings.
export function getSettings(): Settings {
  if (!settings) throw new Error('Settings uninitialized');
  return settings;
}

// Returns the current non-synced settings.
export function getLocalSettings(): LocalSettings {
  if (!localSettings) throw new Error('Settings uninitialized');
  return localSettings;
}

// Writes updated settings to chrome.storage.sync.
// |fn| should make the desired changes to |s|.
export async function updateSettings(fn: (s: Settings) => void) {
  if (!settings) throw new Error('Settings uninitialized');
  const copy = deepCopy(settings);
  fn(copy);
  return chrome.storage.sync.set({ [SETTINGS_KEY]: copy });
}

// Writes updated settings to chrome.storage.local.
// |fn| should make the desired changes to |s|.
export async function updateLocalSettings(fn: (s: LocalSettings) => void) {
  if (!localSettings) throw new Error('Settings uninitialized');
  const copy = deepCopy(localSettings);
  fn(copy);
  return chrome.storage.local.set({ [SETTINGS_KEY]: copy });
}

// Shows the top-level settings dialog.
export function showSettingsDialog() {
  if (!settings || !localSettings) throw new Error('Settings uninitialized');
  const content = cloneTemplate('settings-dialog-template', null);
  const dialog = new Dialog(content);

  // Initialize checkboxes.
  for (const [id, prop] of Object.entries({
    'show-bookmarks-checkbox': 'showBookmarks',
    'show-shortcuts-checkbox': 'showShortcuts',
    'log-to-console-checkbox': 'logToConsole',
  })) {
    if (!Object.hasOwn(settings, prop)) {
      throw new Error(`Missing setting ${prop}`);
    }
    const cb = $<HTMLInputElement>(id);
    cb.checked = !!(settings as any)[prop];
    cb.addEventListener('change', () => {
      updateSettings((s) => ((s as any)[prop] = cb.checked));
    });
  }

  // Initialize color inputs.
  const fgColorInput = $<ColorInput>('fg-color-input');
  const bgColorInput = $<ColorInput>('bg-color-input');
  for (const el of [fgColorInput, bgColorInput]) {
    el.value = el === fgColorInput ? settings!.fgColor : settings!.bgColor;
    el.addEventListener('change', () => {
      checkColors(
        el,
        el === fgColorInput ? bgColorInput : fgColorInput,
        el === fgColorInput ? 'fg' : 'bg'
      );
      updateSettings((s) => {
        // Update both colors in case checkColors() changed the other one.
        s.fgColor = nullIfEmpty(fgColorInput.value);
        s.bgColor = nullIfEmpty(bgColorInput.value);
      });
    });
  }

  // Initialize background-image-related controls.
  const imgFileInput = $<HTMLInputElement>('bg-image-file-input');
  const imgBrowse = $<HTMLButtonElement>('bg-image-browse-button');
  const imgReset = $<HTMLButtonElement>('bg-image-reset-button');
  if (localSettings.bgImageId === null) imgReset.classList.add('hidden');
  imgBrowse.addEventListener('click', () => imgFileInput.click());
  imgFileInput.addEventListener('input', () => {
    const files = imgFileInput.files;
    if (!files || !files.length) return;
    const id = new Date().getTime().toString();
    const startMs = performance.now();
    Promise.all([
      getPlaceholderUrl(files[0]),
      openDb((store) => store.put(files[0], BG_IMAGE_KEY), 'readwrite'),
    ]).then((vals) => {
      if (settings!.logToConsole) {
        console.log(`Stored background in ${sinceMs(startMs)} ms`);
      }
      updateLocalSettings((s) => {
        s.bgImageId = id;
        s.bgImagePlaceholder = vals[0];
      });
      imgReset.classList.remove('hidden');
    });
  });
  imgReset.addEventListener('click', async () => {
    await updateLocalSettings((s) => (s.bgImageId = null));
    openDb((store) => store.delete(BG_IMAGE_KEY), 'readwrite');
    imgReset.classList.add('hidden');
  });

  query(content, '#reset-shortcuts-button').addEventListener('click', () =>
    showResetShortcutsDialog()
  );
  query(content, '.close-button').addEventListener('click', () =>
    dialog.close()
  );
}

// Shows a dialog confirming that the user wants to reset shortcuts.
function showResetShortcutsDialog() {
  if (!settings) throw new Error('Settings uninitialized');
  const content = cloneTemplate('reset-shortcuts-dialog-template', null);
  const dialog = new Dialog(content);
  query(content, '.cancel-button').addEventListener('click', () =>
    dialog.close()
  );
  query(content, '.ok-button').addEventListener('click', () => {
    updateSettings((s) => (s.shortcuts = null));
    dialog.close();
  });
}

// Returns a small placeholder data: URL for |blob|.
const getPlaceholderUrl = (blob: Blob) =>
  new Promise<string>((resolve, reject) => {
    const canvas = document.createElement('canvas');
    const ctx = canvas.getContext('2d')!;
    ctx.imageSmoothingEnabled = true;
    const img = new Image();
    img.onload = () => {
      canvas.width = PLACEHOLDER_WIDTH;
      canvas.height = Math.round(
        (canvas.width * img.naturalHeight) / img.naturalWidth
      );
      ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
      URL.revokeObjectURL(img.src);
      resolve(canvas.toDataURL());
    };
    img.onerror = (ev) => {
      URL.revokeObjectURL(img.src);
      reject(ev);
    };
    img.src = URL.createObjectURL(blob);
  });

// Possibly updates |otherInput| (and more rarely |changedInput|) to try to make
// sure that the UI will be usable.
function checkColors(
  changedInput: ColorInput,
  otherInput: ColorInput,
  changed: 'fg' | 'bg'
) {
  // If the changed color was reset to the default, reset the other color too.
  let changedColor = parseColor(changedInput.value);
  if (!changedColor) {
    otherInput.value = null;
    return;
  }

  // Make sure that the changed color isn't too transparent.
  const minAlpha = changed === 'fg' ? MIN_FG_COLOR_ALPHA : 255;
  if (changedColor.a < minAlpha) {
    changedColor = changedColor.copyWithAlpha(minAlpha);
    changedInput.value = changedColor.toHex();
  }

  // If the other color is unset or very low-contrast, set it to black or white.
  let otherColor = parseColor(otherInput.value);
  if (
    !otherColor ||
    getContrastRatio(changedColor, otherColor) < MIN_CONTRAST_RATIO
  ) {
    otherColor =
      getContrastRatio(changedColor, WHITE) >
      getContrastRatio(changedColor, BLACK)
        ? WHITE
        : BLACK;
    otherInput.value = otherColor.toHex();
  }
}
