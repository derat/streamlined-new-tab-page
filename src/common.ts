// Copyright 2024 Daniel Erat.
// All rights reserved.

// Empty GIF: https://stackoverflow.com/a/14115340
export const emptyImg =
  'data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=';

// Returns the element under |root| with ID |id|.
export function $<T extends HTMLElement = HTMLElement>(
  id: string,
  root: Document | ShadowRoot = document
): T {
  const el = root.getElementById(id);
  if (!el) throw new Error(`Didn't find element #${id}`);
  return el as T;
}

// Calls |parent|'s querySelector() method with |sel|.
// Throws an error if no element is matched.
export function query<T extends Element = HTMLElement>(
  parent: Element | DocumentFragment,
  sel: string
): T {
  const el = parent.querySelector(sel);
  if (!el) throw new Error(`Failed to find element matched by "${sel}"`);
  return el as T;
}

// Gets the specified localized message.
export const $t = chrome.i18n.getMessage;

// Clones the template identified by |id| and returns its first element child.
// If |parent| is supplied, the fragment is appended to it.
// |vals| contains selector-value pairs to set on the fragment.
// '.foo' sets the element's text content.
// '.foo; bar' sets the element's attribute 'bar'.
export function cloneTemplate(
  id: string,
  parent: Element | null = null,
  vals: Record<string, string | null | undefined> = {}
): Element {
  const tmpl = $<HTMLTemplateElement>(id).content;
  const frag = tmpl.cloneNode(true) as DocumentFragment;
  replaceMessages(frag);

  for (const [k, v] of Object.entries(vals)) {
    const parts = k.split(/\s*;\s*/, 2);
    const el = query(frag, parts[0]);
    if (parts.length === 2) {
      if (v !== null && v !== undefined) el.setAttribute(parts[1], v);
      else el.removeAttribute(parts[1]);
    } else {
      el.textContent = v ?? '';
    }
  }
  const el = frag.firstElementChild;
  if (!el) throw new Error(`No element in template ${id}`);
  if (parent) parent.appendChild(frag);
  return el;
}

// Creates and returns a new shadow DOM attached to |el|.
// If |html| is supplied, it is used to set the root node's |innerHTML|.
export function createShadow(el: HTMLElement, html?: string) {
  const shadow = el.attachShadow({ mode: 'open' });
  if (html) {
    shadow.innerHTML = html;
    replaceMessages(shadow);
  }
  return shadow;
}

// Makes a deep copy of |v| by converting it to and from JSON.
export function deepCopy<T>(v: T): T {
  return JSON.parse(JSON.stringify(v));
}

// Freezes |o| recursively.
export function deepFreeze<T>(o: T): T {
  // https://stackoverflow.com/a/69656011
  if (o === null || o === undefined) return o;
  Object.values(o).forEach((v) => Object.isFrozen(v) || deepFreeze(v));
  return Object.freeze(o);
}

// Gets a |size|-by-|size| favicon for |url| from Chrome.
export function getFaviconUrl(url: string, size: number) {
  // https://developer.chrome.com/docs/extensions/how-to/ui/favicons
  const faviconUrl = new URL(chrome.runtime.getURL('/_favicon/'));
  faviconUrl.searchParams.set('pageUrl', url);
  faviconUrl.searchParams.set('size', size.toString());
  return faviconUrl.toString();
}

// Returns true if (x, y) falls within |rect|.
export const inRect = (rect: DOMRect, x: number, y: number) =>
  x >= rect.left && x <= rect.right && y >= rect.top && y <= rect.bottom;

// Returns |s| if non-empty or null otherwise.
export const nullIfEmpty = (s: string | null) => (s !== '' ? s : null);

// Returns the elapsed time between |startMs| and |nowMs| or performance.now().
export const sinceMs = (startMs: number, nowMs: number = performance.now()) =>
  (nowMs - startMs).toFixed();

// Adds a 'click' event listener on |el| if Chrome won't permit navigation to
// |url| via a regular link. The link may be opened either in the current tab or
// a new tab depending on its scheme (sigh).
export function openUrlOnClickIfRestricted(el: Element, url: string) {
  const ms = url.match(/^([^:]+):/); // lenient
  if (!ms) return;
  const handler = restrictedUrlHandlers[ms[1]];
  if (!handler) return;
  el.addEventListener('click', (ev) => {
    ev.preventDefault();
    handler(url);
  });
}

// Map from URL schemes to openers for corresponding URLs.
const restrictedUrlHandlers: Record<string, (url: string) => void> = {
  chrome: (url: string) => chrome.tabs.update({ url }),
  'chrome-extension': (url: string) => chrome.tabs.create({ url }),
  file: (url: string) => {
    chrome.permissions
      .request({ origins: ['file:///*'] })
      .then((granted) =>
        granted
          ? Promise.resolve()
          : Promise.reject(new Error('File permission not granted'))
      )
      .then(() => chrome.tabs.update({ url }));
  },
};

// Adds localized messages to elements under |root|.
// Only elements with 'data-i18n' attributes are modified.
// If the 'data-i18n' attribute contains a value, its text content
// is replaced by the named message. If any of the element's attributes contain
// '__MSG_[name]__' values, they are also replaced by the named messages.
export function replaceMessages(root: Document | DocumentFragment) {
  for (const el of root.querySelectorAll('[data-i18n]')) {
    const name = el.getAttribute('data-i18n') ?? '';
    if (name !== '') el.textContent = chrome.i18n.getMessage(name) ?? '';
    for (const attr of el.attributes) {
      const ms = attr.value.match(/^__MSG_(.+)__$/);
      if (ms) attr.value = chrome.i18n.getMessage(ms[1]);
    }
  }
}
