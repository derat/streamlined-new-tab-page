// This is excerpted from
// https://github.com/DefinitelyTyped/DefinitelyTyped/blob/master/types/chrome/index.d.ts.

interface Window {
  chrome: typeof chrome;
}

declare namespace chrome.bookmarks {
  export interface BookmarkTreeNode {
    index?: number | undefined;
    dateAdded?: number | undefined;
    title: string;
    url?: string | undefined;
    dateGroupModified?: number | undefined;
    id: string;
    parentId?: string | undefined;
    children?: BookmarkTreeNode[] | undefined;
    unmodifiable?: 'managed' | undefined;
  }

  export function get(id: string): Promise<BookmarkTreeNode[]>;
  export function getChildren(id: string): Promise<BookmarkTreeNode[]>;
  export function getSubTree(id: string): Promise<BookmarkTreeNode[]>;

  export interface BookmarkChangeInfo {
    url?: string | undefined;
    title: string;
  }
  export interface BookmarkMoveInfo {
    index: number;
    oldIndex: number;
    parentId: string;
    oldParentId: string;
  }
  export interface BookmarkRemoveInfo {
    index: number;
    parentId: string;
    node: BookmarkTreeNode;
  }
  export interface BookmarkReorderInfo {
    childIds: string[];
  }

  export interface BookmarkChangedEvent
    extends chrome.events.Event<
      (id: string, changeInfo: BookmarkChangeInfo) => void
    > {}
  export interface BookmarkChildrenReordered
    extends chrome.events.Event<
      (id: string, reorderInfo: BookmarkReorderInfo) => void
    > {}
  export interface BookmarkCreatedEvent
    extends chrome.events.Event<
      (id: string, bookmark: BookmarkTreeNode) => void
    > {}
  export interface BookmarkMovedEvent
    extends chrome.events.Event<
      (id: string, moveInfo: BookmarkMoveInfo) => void
    > {}
  export interface BookmarkRemovedEvent
    extends chrome.events.Event<
      (id: string, removeInfo: BookmarkRemoveInfo) => void
    > {}

  export var onChanged: BookmarkChangedEvent;
  export var onChildrenReordered: BookmarkChildrenReordered;
  export var onCreated: BookmarkCreatedEvent;
  export var onMoved: BookmarkMovedEvent;
  export var onRemoved: BookmarkRemovedEvent;
}

declare namespace chrome.events {
  interface Event<T extends Function> {
    addListener(callback: T): void;
  }
}

declare namespace chrome.i18n {
  export function getMessage(
    messageName: string,
    substitutions?: string | string[]
  ): string;
}

declare namespace chrome.permissions {
  export interface Permissions {
    permissions?: string[] | undefined;
    origins?: string[] | undefined;
  }
  export function request(permissions: Permissions): Promise<boolean>;
}

declare namespace chrome.runtime {
  export function getURL(path: string): string;
}

declare namespace chrome.storage {
  export interface StorageArea {
    set(items: { [key: string]: any }): Promise<void>;
    get(
      keys?: string | string[] | { [key: string]: any } | null
    ): Promise<{ [key: string]: any }>;
    remove(keys: string | string[]): Promise<void>;
    onChanged: StorageAreaChangedEvent;
  }
  export interface StorageChange {
    newValue?: any;
    oldValue?: any;
  }
  export interface StorageAreaChangedEvent
    extends chrome.events.Event<
      (changes: { [key: string]: StorageChange }) => void
    > {}
  export interface SyncStorageArea extends StorageArea {}
  export var sync: SyncStorageArea;
  export interface LocalStorageArea extends StorageArea {}
  export var local: LocalStorageArea;
}

declare namespace chrome.tabs {
  export interface CreateProperties {
    url?: string | undefined;
  }
  export interface UpdateProperties {
    url?: string | undefined;
  }
  export interface Tab {}
  export function create(createProperties: CreateProperties): Promise<Tab>;
  export function update(updateProperties: UpdateProperties): Promise<Tab>;
}

declare namespace chrome.topSites {
  export interface MostVisitedURL {
    url: string;
    title: string;
  }
  export function get(): Promise<MostVisitedURL[]>;
}
