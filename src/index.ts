// Copyright 2024 Daniel Erat.
// All rights reserved.

import {
  listenForBookmarksBarChanges,
  updateBookmarksBar,
  updateBookmarksBarVisibility,
} from './bookmarks.js';
import { $, nullIfEmpty, replaceMessages, sinceMs } from './common.js';
import { getFromDb } from './db.js';
import {
  BG_IMAGE_KEY,
  getSettings,
  getLocalSettings,
  initSettings,
  showSettingsDialog,
} from './settings.js';
import { updateShortcuts } from './shortcuts.js';

// Import web components.
import './color-input.js';

// Fade from the placeholder background image to the real one if it takes longer
// than this for the real image to be loaded and decoded.
const BG_IMAGE_FADE_DELAY_MS = 100;

// Initialize the page.
(() => {
  // Insert localized messages. Note that this doesn't update templates;
  // those are handled in createTemplate() instead.
  replaceMessages(document);

  const startMs = performance.now();
  let initialized = false;
  initSettings(() => {
    if (!initialized) {
      if (getSettings().logToConsole) {
        console.log(`Loaded settings in ${sinceMs(startMs)} ms`);
      }
      $('settings-icon').addEventListener('click', () => showSettingsDialog());
      $('settings-icon').addEventListener('keydown', (ev) => {
        if (ev.key === 'Enter') showSettingsDialog();
      });
      listenForBookmarksBarChanges(() => updateBookmarksBar());
      updateBookmarksBar();
      initialized = true;
    } else {
      // Just update visibility for settings changes to avoid jank.
      updateBookmarksBarVisibility();
    }
    updateShortcuts();
    updateColors();
    updateBgImage();
  });
})();

// Updates color-related CSS vars based on settings.
function updateColors() {
  const settings = getSettings();
  document.body.style.setProperty('--bg-color', nullIfEmpty(settings.bgColor));
  document.body.style.setProperty('--fg-color', nullIfEmpty(settings.fgColor));
}

// Updates the background image based on settings.
async function updateBgImage() {
  const settings = getLocalSettings();
  const curId = document.body.getAttribute('bg-image');
  const newId = nullIfEmpty(settings.bgImageId);
  if (curId === newId) return;

  const img = $<HTMLImageElement>('bg-image');
  if (img.src) URL.revokeObjectURL(img.src);
  img.classList.remove('shown', 'slow');

  document.body.style.backgroundImage = '';
  document.body.removeAttribute('bg-image');

  if (newId === null) return;

  // Display the placeholder data: URL from chrome.storage.local while we're
  // waiting for the possibly-slow full-resolution image to be loaded from
  // IndexedDB and decoded.
  document.body.style.backgroundImage = `url("${settings.bgImagePlaceholder ?? ''}")`;
  document.body.setAttribute('bg-image', newId);

  const startMs = performance.now();
  const blob = (await getFromDb(BG_IMAGE_KEY)) as Blob;
  if (!blob) {
    console.error('Failed getting background image from IndexedDB');
    document.body.removeAttribute('bg-image');
    return;
  }
  const loadedMs = performance.now();

  const blobUrl = URL.createObjectURL(blob);
  window.setTimeout(() => {
    // Smoothly transition from the placeholder if it took a while to get the
    // real image ready.
    if (!img.classList.contains('shown')) img.classList.add('slow');
  }, BG_IMAGE_FADE_DELAY_MS);
  img.src = blobUrl;
  img.decode().then(() => {
    img.classList.add('shown');
    if (getSettings().logToConsole) {
      console.log(
        `Displayed background in ${sinceMs(startMs)} ms ` +
          `(load ${sinceMs(startMs, loadedMs)} ms, ` +
          `decode ${sinceMs(loadedMs)} ms)`
      );
    }
  });
}
