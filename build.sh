#!/bin/bash -e

ESBUILD_IN=src/index.ts
ESBUILD_OUT=page.js
ESBUILD_TARGET=esnext

SASS_IN=css/index.scss
SASS_OUT=page.css

usage() {
  cat <<EOF >&2
Usage: build.sh [flags]...

Flags:
  -o package.zip  Create a zip file after building
  -w              Watch for changes and rebuild

EOF
  exit 2
}

out=
watch=
while getopts 'ho:w' option; do
  case "$option" in
    o)
      out="$OPTARG"
      ;;
    w)
      watch=1
      ;;
    *)
      usage
      ;;
  esac
done
shift $((OPTIND-1))

[[ $# -eq 0 ]] || usage

if [[ -n $out && -n $watch ]]; then
  echo "-o and -w are mutually exclusive" >&2
  exit 2
fi

if [[ -n $out && $out != *.zip ]]; then
  echo "-o arg should have .zip extension" >&2
  exit 2
fi

for exe in entr esbuild sassc; do
  if ! command -v "$exe" >/dev/null 2>&1; then
    echo "Executable ${exe} not in path" >&2
    exit 2
  fi
done

run_esbuild() {
  local args=(--minify)
  [[ -n $watch ]] && args=(--watch=forever --sourcemap)
  esbuild --bundle --target="$ESBUILD_TARGET" "${args[@]}" \
    --outfile="$ESBUILD_OUT" "$ESBUILD_IN"
}

run_sass() {
  if [[ -z $watch ]]; then
    sassc --style compressed "$SASS_IN" "$SASS_OUT"
  else
    # https://github.com/sass/sassc/issues/63#issuecomment-515623032
    local args=(--style expanded --sourcemap "$SASS_IN" "$SASS_OUT")
    sassc "${args[@]}" || true
    ls "$SASS_IN" | entr -d -n sassc "${args[@]}"
  fi
}

# https://stackoverflow.com/a/52033580/6882947
(trap 'kill 0' SIGINT; run_esbuild & run_sass & wait)

if [[ -z $watch ]]; then ./check_messages.sh; fi

if [[ -n $out ]]; then
  zip -r "$out" \
    manifest.json \
    ./*.css \
    ./*.html \
    ./*.js \
    ./icons/*.png \
    _locales \
    README.md \
    LICENSE
fi
