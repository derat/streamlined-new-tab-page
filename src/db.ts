// Copyright 2024 Daniel Erat.
// All rights reserved.

const DB_NAME = 'Database';
const DB_VERSION = 1;
const STORE_NAME = 'Store';

// Opens IndexedDB and passes the object store to |cb|.
export const openDb = (
  cb: (store: IDBObjectStore) => void,
  mode: 'readonly' | 'readwrite' = 'readonly'
) =>
  new Promise<void>((resolve, reject) => {
    const req = window.indexedDB.open(DB_NAME, DB_VERSION);
    req.onupgradeneeded = () => {
      const db = req.result;
      db.createObjectStore(STORE_NAME);
    };
    req.onsuccess = () => {
      const db = req.result;
      const tx = db.transaction(STORE_NAME, mode);
      tx.oncomplete = () => db.close();
      cb(tx.objectStore(STORE_NAME));
      tx.commit();
      resolve();
    };
    req.onerror = (ev) => {
      reject(ev);
    };
  });

// Gets |key| from IndexedDB.
export const getFromDb = (key: string) =>
  new Promise<any>((resolve, reject) => {
    openDb((store) => {
      const req = store.get(key);
      req.onsuccess = () => resolve(req.result);
      req.onerror = (ev) => reject(ev);
    });
  });
