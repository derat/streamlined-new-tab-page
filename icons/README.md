# icons

Extension icon sizing is dumb.

`chrome://extensions` displays icons at 36x36 CSS pixels. If 48x48 is supplied,
it always uses that, which looks like trash on high-DPI displays. If 48x48 isn't
supplied but 128x128 is, it uses 128x128 instead (even if 16x16 and 32x32 are
supplied).

`chrome://extensions/?id=...` displays icons at just 24x24 CSS pixels and uses
the same logic to choose which image to use.

The Chrome extension menu looks like it favors 16x16 or 32x32 (I'm not sure
which). It falls back to 128x128 if that's all that's supplied.

<https://developer.chrome.com/docs/extensions/reference/manifest/icons> says
that Chrome also displays the 128x128 icon during installation.

`https://chromewebstore.google.com/detail/...` seems to scale 128x128 icons down
to 60x60 and then display them at 60x60 CSS pixels, regardless of pixel density.
Someone at Google should sign off on buying high-DPI displays so the Web Store
engineers can test their site there.

So, I think that the best practices are to include 16x16 and 32x32 icons with no
margin for the extension menu, and 128x128 (plus 256x256 to future-proof?) icons
with a margin for `chrome://extension` pages. The 128x128 icon is regrettably
downscaled to 24x24 on the detail page, but that seems better than having a
blurry icon show up on `chrome://extensions`.
